//console.log("Hello World")
let num = prompt("Enter a number")

for(let n = num; n >= 0; n--) {
	
	if((n % 10) == 0)
		console.log("The number is divisible by 10. Skipping the number")
	else if ((n % 5) == 0) {
		console.log(n)
		continue
	}
	if(n < 50 ) {
		break
	}
};

let word = "supercalifragilisticexpialidocous";
console.log(word)
let consonants = "";
for (let w = 0; w < word.length; w++) {
	if(
		word[w] == "a" ||
		word[w] == "e" ||
		word[w] == "i" ||
		word[w] == "o" ||
		word[w] == "u" 
	) {continue}

	else {
		consonants += word[w]
	}
	
}
console.log(consonants);